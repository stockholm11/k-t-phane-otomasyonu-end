﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Proke.Models;

namespace Web_Proke.Areas.AdminPanel.Controllers
{
    
    public class AdminKatalogController : Controller
    {
        private KutuphaneEntitiesCompactDBEntities db = new KutuphaneEntitiesCompactDBEntities();

        // GET: AdminPanel/AdminKatalog
        public ActionResult Index()
        {
            var katalog = db.Katalog.Include(k => k.Konum);
            return View(katalog.ToList());
        }

        // GET: AdminPanel/AdminKatalog/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Katalog katalog = db.Katalog.Find(id);
            if (katalog == null)
            {
                return HttpNotFound();
            }
            return View(katalog);
        }

        // GET: AdminPanel/AdminKatalog/Create
        public ActionResult Create()
        {
            ViewBag.KonumID = new SelectList(db.Konum, "KonumID", "KonumID");
            return View();
        }

        // POST: AdminPanel/AdminKatalog/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KatalogID,YayinAdı,KonuBasliklari,Tür,YayinBilgisi,FizikselNitelik,ISBN_ISSN,KonumID")] Katalog katalog)
        {
            if (ModelState.IsValid)
            {
                db.Katalog.Add(katalog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KonumID = new SelectList(db.Konum, "KonumID", "KonumID", katalog.KonumID);
            return View(katalog);
        }

        // GET: AdminPanel/AdminKatalog/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           Katalog katalog = db.Katalog.Find(id);
            if (katalog == null)
            {
                return HttpNotFound();
            }
            ViewBag.KonumID = new SelectList(db.Konum, "KonumID", "KonumID", katalog.KonumID);
            return View(katalog);
        }

        // POST: AdminPanel/AdminKatalog/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KatalogID,YayinAdı,KonuBasliklari,Tür,YayinBilgisi,FizikselNitelik,ISBN_ISSN,KonumID")] Katalog katalog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(katalog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KonumID = new SelectList(db.Konum, "KonumID", "KonumID", katalog.KonumID);
            return View(katalog);
        }

        // GET: AdminPanel/AdminKatalog/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           Katalog katalog = db.Katalog.Find(id);
            if (katalog == null)
            {
                return HttpNotFound();
            }
            return View(katalog);
        }

        // POST: AdminPanel/AdminKatalog/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Katalog katalog = db.Katalog.Find(id);
            db.Katalog.Remove(katalog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
