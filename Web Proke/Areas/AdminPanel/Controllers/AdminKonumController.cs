﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Proke.Models;

namespace Web_Proke.Areas.AdminPanel.Controllers
{
   
    public class AdminKonumController : Controller
    {
        private KutuphaneEntitiesCompactDBEntities db = new KutuphaneEntitiesCompactDBEntities();

        // GET: AdminPanel/AdminKonum
        public ActionResult Index()
        {
            return View(db.Konum.ToList());
        }

        // GET: AdminPanel/AdminKonum/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Konum konum = db.Konum.Find(id);
            if (konum == null)
            {
                return HttpNotFound();
            }
            return View(konum);
        }

        // GET: AdminPanel/AdminKonum/Create
        public ActionResult Create()
        {
            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KonumID,DolapKodu,RafNo,RafSira")] Konum konum)
        {
            if (ModelState.IsValid)
            {
                db.Konum.Add(konum);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(konum);
        }

        // GET: AdminPanel/AdminKonum/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Konum konum = db.Konum.Find(id);
            if (konum == null)
            {
                return HttpNotFound();
            }
            return View(konum);
        }

        // POST: AdminPanel/AdminKonum/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KonumID,DolapKodu,RafNo,RafSira")] Konum konum)
        {
            if (ModelState.IsValid)
            {
                db.Entry(konum).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(konum);
        }

        // GET: AdminPanel/AdminKonum/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Konum konum = db.Konum.Find(id);
            if (konum == null)
            {
                return HttpNotFound();
            }
            return View(konum);
        }

        // POST: AdminPanel/AdminKonum/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           Konum konum = db.Konum.Find(id);
            db.Konum.Remove(konum);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
