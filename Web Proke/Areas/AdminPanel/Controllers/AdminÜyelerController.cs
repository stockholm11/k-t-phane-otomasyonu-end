﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Proke.Models;

namespace Web_Proke.Areas.AdminPanel.Controllers
{
   
    public class AdminÜyelerController : Controller
    {
        private KutuphaneEntitiesCompactDBEntities db = new KutuphaneEntitiesCompactDBEntities();

        // GET: AdminPanel/AdminÜyeler
        public ActionResult Index()
        {
            return View(db.Üyeler.ToList());
        }

        // GET: AdminPanel/AdminÜyeler/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Üyeler üyeler = db.Üyeler.Find(id);
            if (üyeler == null)
            {
                return HttpNotFound();
            }
            return View(üyeler);
        }

        // GET: AdminPanel/AdminÜyeler/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminPanel/AdminÜyeler/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ÜyeID,ÜyeAdiSoyadi,TelefonNo,a_mail,KullanıcıAdi,Parola,ParolaTekrar")] Üyeler üyeler)
        {
            if (ModelState.IsValid)
            {
                üyeler.KayitTarihi = DateTime.Now;
                db.Üyeler.Add(üyeler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(üyeler);
        }

        // GET: AdminPanel/AdminÜyeler/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Üyeler üyeler = db.Üyeler.Find(id);
            üyeler.KayitTarihi = DateTime.Now;
            if (üyeler == null)
            {
                return HttpNotFound();
            }
            return View(üyeler);
        }

        // POST: AdminPanel/AdminÜyeler/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ÜyeID,ÜyeAdiSoyadi,TelefonNo,a_mail,KullanıcıAdi,Parola,ParolaTekrar")] Üyeler üyeler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(üyeler).State = EntityState.Modified;
                üyeler.KayitTarihi = DateTime.Now;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(üyeler);
        }

        // GET: AdminPanel/AdminÜyeler/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Üyeler üyeler = db.Üyeler.Find(id);
            if (üyeler == null)
            {
                return HttpNotFound();
            }
            return View(üyeler);
        }

        // POST: AdminPanel/AdminÜyeler/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Üyeler üyeler = db.Üyeler.Find(id);
            db.Üyeler.Remove(üyeler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
