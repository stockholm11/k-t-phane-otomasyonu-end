﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Proke.Models;

namespace Web_Proke.Controllers
{
    public class DenemeController : Controller
    {
        KutuphaneEntitiesCompactDBEntities db = new KutuphaneEntitiesCompactDBEntities();
        public ActionResult KisiEkle()
        {
            var kisi = new Üyeler();
            return View(kisi);
        }
        [HttpPost]
        public ActionResult KisiEkle(Üyeler kisi)
        {
            kisi.KayitTarihi = DateTime.Now;
            db.Üyeler.Add(kisi);
            db.SaveChanges();
            return Redirect("Home/Index");

        }
    }
}