﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Proke.Models;

namespace Web_Proke.Controllers
{

    public class HomeController : Controller
    {

        private KutuphaneEntitiesCompactDBEntities db = new KutuphaneEntitiesCompactDBEntities();
        public ActionResult Index()
        {
            Random r1 = new Random();
            int d1 = r1.Next(6, 8);

            ViewBag.duyuru1 = db.Duyurular.Find(5).Duyuru;
            ViewBag.duyuru2 = db.Duyurular.Find(d1).Duyuru;

            return View();
        }


        public ActionResult Hakkımızda()
        {


            return View();
        }

        public ActionResult İletisim()
        {


            return View();
        }

        public ActionResult Fotograf()
        {
            return View();
        }

        public ActionResult CalismaSaatleri()
        {
            return View();
        }
        public ActionResult ChangeCulture(string lang, string returnUrl)
        { Session["Culture"] = new CultureInfo(lang); return Redirect(returnUrl); }
    }
}