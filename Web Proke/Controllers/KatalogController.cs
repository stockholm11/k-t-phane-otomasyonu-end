﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Proke.Models;

namespace Web_Proke.Controllers
{
 

    public class KatalogController : Controller
    {
        private KutuphaneEntitiesCompactDBEntities db = new KutuphaneEntitiesCompactDBEntities();

        //public ActionResult Listele(string katalog)
        //{

        //    return View(katalog.ToList());
        //}
        // GET: Katalog
        public ActionResult Kitaplar()
        {
            var katalog = db.Katalog.Where(x => x.Tür == "Kitap");
            return View(katalog.ToList());
        }
        [HttpPost]
        public ActionResult Kitaplar(string aranan)
        {
            var katalog = db.Katalog.Where(x => x.YayinAdı == aranan);
            return View(katalog.ToList());
        }
      
  

        // GET: Katalog/Details/5
        public ActionResult Ansiklopediler()
        {
            var katalog = db.Katalog.Where(x => x.Tür == "Ansiklopedi");
            return View(katalog.ToList());
        }

        // GET: Katalog/Create
        public ActionResult Makaleler()
        {
            var katalog = db.Katalog.Where(x => x.Tür == "Makale");
            return View(katalog.ToList());
        }

   
        // GET: Katalog/Edit/5
        public ActionResult Dergiler()
        {
            var katalog = db.Katalog.Where(x => x.Tür == "Dergi");
            return View(katalog.ToList());
        }

  
        // GET: Katalog/Delete/5
        public ActionResult DigitalKayitlar(int? id)
        {
            var katalog = db.Katalog.Where(x => x.Tür == "Digital Kayit");
            return View(katalog.ToList());
        }


      
    }
}
