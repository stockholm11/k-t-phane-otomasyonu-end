﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Web_Proke.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class lang {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal lang() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Web_Proke.Resources.lang", typeof(lang).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Adı.
        /// </summary>
        public static string ad {
            get {
                return ResourceManager.GetString("ad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ad Soyad.
        /// </summary>
        public static string ad_soyad {
            get {
                return ResourceManager.GetString("ad_soyad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ana Sayfa.
        /// </summary>
        public static string anasayfa {
            get {
                return ResourceManager.GetString("anasayfa", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ansiklopediler.
        /// </summary>
        public static string ansiklopedi {
            get {
                return ResourceManager.GetString("ansiklopedi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ansiklopediler.
        /// </summary>
        public static string ansiklopediler {
            get {
                return ResourceManager.GetString("ansiklopediler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çalışma Saatleri.
        /// </summary>
        public static string c1 {
            get {
                return ResourceManager.GetString("c1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to a) Akademik personel 30 gün süreyle 10 kitap, idari personel ve lisansüstü öğrenciler 20 gün süreyle 5 kitap, önlisans ve özel statüdeki (Erasmus, Farabi v.b.) öğrenciler 15 gün süreyle 3 kitap ödünç alabilir.
        /// </summary>
        public static string c10 {
            get {
                return ResourceManager.GetString("c10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to b) Okuyucular üniversite kimliğini göstererek ödünç kitap alabilirler. Başka okuyucunun üniversite kimliği kullanılarak ödünç kitap alınmaz.
        /// </summary>
        public static string c11 {
            get {
                return ResourceManager.GetString("c11", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to c) Ödünç alınan materyalin süresi, başka istekli yok ise 2 kez daha uzatılır. İnternet üzerinden uzatma işlemi iade gününden 3 gün öncesinden yapılabilmektedir. Bunun için katalog tarama üyeler kısmından özel oturum açmak gerekmektedir.
        /// </summary>
        public static string c12 {
            get {
                return ResourceManager.GetString("c12", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to d) Ödünç aldıkları materyali zamanında iade etmeyen okuyucular tekrar ödünç materyal alamazlar.
        /// </summary>
        public static string c13 {
            get {
                return ResourceManager.GetString("c13", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to e) Gerektiğinde Daire Başkanlığı, ödünç verme süresi dolmadan okuyuculardan materyalleri geri isteyebilir.
        /// </summary>
        public static string c14 {
            get {
                return ResourceManager.GetString("c14", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to f) Kütüphaneden ödünç aldığı kaynakları kaybeden okuyucular, kaybettiği kaynağın aynısını sağlamak zorundadırlar. Kaybolan kitabın aynısı temin edilmemiş ise kurulan bir Takdir Komisyonunca kitabın o günkü değeri saptanarak, gecikme cezasıyla okuyucuya ödetilir.
        /// </summary>
        public static string c15 {
            get {
                return ResourceManager.GetString("c15", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to g) Herhangi bir nedenle Üniversiteden ayrılan üyelerin, kütüphaneden ilişiklerini kesmesi gerekmektedir. Aldığı kaynağı iade etmeden ayrılan veya öğrenimini yarıda bırakarak ayrılan üyeler için yasal işlemler yapılır.
        /// </summary>
        public static string c16 {
            get {
                return ResourceManager.GetString("c16", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to h) Kütüphane materyalini izinsiz olarak kütüphane dışına çıkartılmaya çalışılması durumunda yasal işlem uygulanır.
        /// </summary>
        public static string c17 {
            get {
                return ResourceManager.GetString("c17", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to i) Kütüphaneye üye olan tüm okuyucular, kütüphane ödünç alma koşullarını kabul etmiş sayılırlar.
        /// </summary>
        public static string c18 {
            get {
                return ResourceManager.GetString("c18", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kategoriler.
        /// </summary>
        public static string c19 {
            get {
                return ResourceManager.GetString("c19", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sakarya Üniversitesi Kütüphanesi Çalışma Saatleri.
        /// </summary>
        public static string c2 {
            get {
                return ResourceManager.GetString("c2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ÖĞRETİM DÖNEMİ.
        /// </summary>
        public static string c3 {
            get {
                return ResourceManager.GetString("c3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to YAZ DÖNEMİ.
        /// </summary>
        public static string c4 {
            get {
                return ResourceManager.GetString("c4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Merkez Kütüphane 7/24 açıktır.
        /// </summary>
        public static string c5 {
            get {
                return ResourceManager.GetString("c5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hafta içi   08:30 - 17:30.
        /// </summary>
        public static string c6 {
            get {
                return ResourceManager.GetString("c6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to KÜTÜPHANEDEN YARARLANMA.
        /// </summary>
        public static string c7 {
            get {
                return ResourceManager.GetString("c7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Üniversitemiz kütüphanelerinden Sakarya Üniversitesi akademik ve idari personel, öğrenciler ile dışarıdan gelen araştırmacılar yararlanabilir. Dışarıdan gelen araştırmacılara ödünç kitap verilmemekte, fotokopi çekilmesine izin verilmektedir.
        /// </summary>
        public static string c8 {
            get {
                return ResourceManager.GetString("c8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ÖDÜNÇ VERME ESASLARI.
        /// </summary>
        public static string c9 {
            get {
                return ResourceManager.GetString("c9", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çalışan Adı.
        /// </summary>
        public static string cal_ad {
            get {
                return ResourceManager.GetString("cal_ad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çalışma Saatleri.
        /// </summary>
        public static string cal_saat {
            get {
                return ResourceManager.GetString("cal_saat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çalışan Soyadı.
        /// </summary>
        public static string cal_soyad {
            get {
                return ResourceManager.GetString("cal_soyad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çalışanlar.
        /// </summary>
        public static string calisanlar {
            get {
                return ResourceManager.GetString("calisanlar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İletişim.
        /// </summary>
        public static string d1 {
            get {
                return ResourceManager.GetString("d1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mesajınız.
        /// </summary>
        public static string d10 {
            get {
                return ResourceManager.GetString("d10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mesajı Gönder.
        /// </summary>
        public static string d11 {
            get {
                return ResourceManager.GetString("d11", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bize Nasıl Ulaşabilirsiniz.
        /// </summary>
        public static string d2 {
            get {
                return ResourceManager.GetString("d2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sakarya Üniversitesi Esentepe Kampüsü.
        /// </summary>
        public static string d3 {
            get {
                return ResourceManager.GetString("d3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Merkez Kütüphanesi.
        /// </summary>
        public static string d4 {
            get {
                return ResourceManager.GetString("d4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mesajınızı Bırakabilirsiniz.
        /// </summary>
        public static string d5 {
            get {
                return ResourceManager.GetString("d5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to * olan yerleri doldurmak zorunludur.
        /// </summary>
        public static string d6 {
            get {
                return ResourceManager.GetString("d6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ad.
        /// </summary>
        public static string d7 {
            get {
                return ResourceManager.GetString("d7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Soyad.
        /// </summary>
        public static string d8 {
            get {
                return ResourceManager.GetString("d8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Konu.
        /// </summary>
        public static string d9 {
            get {
                return ResourceManager.GetString("d9", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dergiler.
        /// </summary>
        public static string dergi {
            get {
                return ResourceManager.GetString("dergi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dergiler.
        /// </summary>
        public static string dergiler {
            get {
                return ResourceManager.GetString("dergiler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Digital Kayıtlar.
        /// </summary>
        public static string digital_kayit {
            get {
                return ResourceManager.GetString("digital_kayit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dergiler.
        /// </summary>
        public static string dirgiler {
            get {
                return ResourceManager.GetString("dirgiler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dolap Kodu.
        /// </summary>
        public static string dolap_kodu {
            get {
                return ResourceManager.GetString("dolap_kodu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Durum.
        /// </summary>
        public static string durum {
            get {
                return ResourceManager.GetString("durum", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Duyuru.
        /// </summary>
        public static string duyuru {
            get {
                return ResourceManager.GetString("duyuru", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Duyuru Konusu.
        /// </summary>
        public static string duyuru_konu {
            get {
                return ResourceManager.GetString("duyuru_konu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Duyuru Tarihi.
        /// </summary>
        public static string duyuru_tarih {
            get {
                return ResourceManager.GetString("duyuru_tarih", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to e posta.
        /// </summary>
        public static string e_posta {
            get {
                return ResourceManager.GetString("e_posta", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to E posta.
        /// </summary>
        public static string e_postaa {
            get {
                return ResourceManager.GetString("e_postaa", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kütüphanemizden Kareler.
        /// </summary>
        public static string f1 {
            get {
                return ResourceManager.GetString("f1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Üniversitemiz kütüphanelerinden Sakarya Üniversitesi akademik ve idari personel, öğrenciler ile dışarıdan gelen araştırmacılar yararlanabilir.  &lt;br&gt; Dışarıdan gelen araştırmacılara ödünç kitap verilmemekte, fotokopi çekilmesine izin verilmektedir.
        /// </summary>
        public static string f2 {
            get {
                return ResourceManager.GetString("f2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hepsi.
        /// </summary>
        public static string f3 {
            get {
                return ResourceManager.GetString("f3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İçerden Görünüm.
        /// </summary>
        public static string f4 {
            get {
                return ResourceManager.GetString("f4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Her kattan.
        /// </summary>
        public static string f5 {
            get {
                return ResourceManager.GetString("f5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çalışma Ortamları.
        /// </summary>
        public static string f6 {
            get {
                return ResourceManager.GetString("f6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fizilksel Nitelik.
        /// </summary>
        public static string fiz_nitelik {
            get {
                return ResourceManager.GetString("fiz_nitelik", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fotoğraflar.
        /// </summary>
        public static string fotograf {
            get {
                return ResourceManager.GetString("fotograf", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Görevi.
        /// </summary>
        public static string görev {
            get {
                return ResourceManager.GetString("görev", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Misyon ve Vizyon.
        /// </summary>
        public static string h1 {
            get {
                return ResourceManager.GetString("h1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Misyonumuz
        ///
        ///                    “İnsanlığa değer katan girişimci bireyler yetiştirmek ve evrensel nitelikte bilgi, teknoloji ve hizmet üretmektir.
        /// </summary>
        public static string h2 {
            get {
                return ResourceManager.GetString("h2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vizyonumuz
        ///
        ///                    “Ürettiği evrensel nitelikte bilgi ve teknoloji ile Türkiye ve dünyada paydaşlarının geleceğine yön veren bir üniversite olmaktır.
        /// </summary>
        public static string h3 {
            get {
                return ResourceManager.GetString("h3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tarihçe.
        /// </summary>
        public static string h4 {
            get {
                return ResourceManager.GetString("h4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kendisine çağdaş uygarlığın gerektirdiği her türlü donanıma sahip bireyler yetiştirmeyi amaç edinmiş olan Sakarya Üniversitesi&apos;nin çekirdeğini 1970 yılında açılan Sakarya Mühendislik ve Mimarlık Yüksekokulu oluşturmuştur. Bu okul 1971 yılında Sakarya Devlet Mimarlık ve Mühendislik Akademisi&apos;ne dönüşmüş, 1982-1992 yılları arasında İstanbul Teknik Üniversitesi&apos;ne bağlı bir Fakülte olarak öğretim vermiştir. &lt;br&gt;3 Temmuz 1992 tarih ve 3837 sayılı kanun ile Sakarya Üniversitesi kurulmuştur.&lt;br&gt; 90 sonrasında kur [rest of string was truncated]&quot;;.
        /// </summary>
        public static string h5 {
            get {
                return ResourceManager.GetString("h5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fiziki imkanlar.
        /// </summary>
        public static string h6 {
            get {
                return ResourceManager.GetString("h6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Üniversitemiz yerleşkesi yan kampüslerimiz de dahil yaklaşık 1.700,000 m2’dir. Eğitim öğretim ve idari süreçlerinin uygulandığı fiziki alan toplamı ise 320.433.9 m2’dir. &lt;br&gt; 2013-2014 yılı itibariyle Üniversitemiz bünyesinde 16 Fakülte, 7 Yüksek Okul, 18 Meslek Yüksek Okulu, 4 Enstitü ve Devlet Konservatuvarı eğitim ve öğretim birimleri mevcut olup, toplam öğrenci sayımız  62.719’dur.&lt;br&gt; Bugün itibariyle devam eden inşaatlarımız  Kampüs Alt Yapı Projesi kapsamında :
        ///                        4 adet Laborat [rest of string was truncated]&quot;;.
        /// </summary>
        public static string h7 {
            get {
                return ResourceManager.GetString("h7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hakkımızda.
        /// </summary>
        public static string hakkımızda {
            get {
                return ResourceManager.GetString("hakkımızda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Üniversitemiz Kütüphanelerine satın alınacak kitaplar için Mart, Haziran ve Eylül aylarında ihale yapılması planlanmıştır.
        /// </summary>
        public static string i1 {
            get {
                return ResourceManager.GetString("i1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Merkez Kütüphane bulunan basılı materyallerden ücret karşılığı fotokopi çekilmektedir..
        /// </summary>
        public static string i10 {
            get {
                return ResourceManager.GetString("i10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kütüphaneye Türkçe ve yabancı dilde süreli yayınlar gelmektedir. Dergilerin otomasyon programına yüklenerek bilgisayar aracılığıyla kullanıcılara duyurulma çalışmaları devam etmekte ve raflarda tüm sayılarıyla birlikte sergilenmektedir..
        /// </summary>
        public static string i11 {
            get {
                return ResourceManager.GetString("i11", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Merkez Kütüphanede Öğretim Elemanlarının ve öğrencilerin kullanabileceği Internet bağlantılı bilgisayar ve Internet odası mevcuttur. Bu odada yayın tarama, tez, disket veya CD&apos;den çıktı alma hizmetleri belirli bir ücret karşılığı verilmektedir..
        /// </summary>
        public static string i12 {
            get {
                return ResourceManager.GetString("i12", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kütüphanemiz.
        /// </summary>
        public static string i13 {
            get {
                return ResourceManager.GetString("i13", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kütüphanemiz üniversitemizin öğretim elemanları, öğrenci ve personelinin.
        /// </summary>
        public static string i14 {
            get {
                return ResourceManager.GetString("i14", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to eğitim, öğretim, araştırma programlarını desteklemek ve boş zamanlarını değerlendirmeleri için.
        /// </summary>
        public static string i15 {
            get {
                return ResourceManager.GetString("i15", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to her türlü bilgiyi ve bilgi kaynaklarını toplayıp kullanıcıların hizmetine en iyi şekilde sunan merkezdir..
        /// </summary>
        public static string i16 {
            get {
                return ResourceManager.GetString("i16", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sakarya Üniversitesi.
        /// </summary>
        public static string i17 {
            get {
                return ResourceManager.GetString("i17", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kütüphanesi.
        /// </summary>
        public static string i18 {
            get {
                return ResourceManager.GetString("i18", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Duyuru.
        /// </summary>
        public static string i19 {
            get {
                return ResourceManager.GetString("i19", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Verilen Hizmetler.
        /// </summary>
        public static string i2 {
            get {
                return ResourceManager.GetString("i2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Yayınlar.
        /// </summary>
        public static string i20 {
            get {
                return ResourceManager.GetString("i20", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Kütüphanemiz.
        /// </summary>
        public static string i21 {
            get {
                return ResourceManager.GetString("i21", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sakarya Üniversitesinin bilimsel çalışmaların daha ferah ve teknolojik bir ortamda yapılması adına.
        /// </summary>
        public static string i22 {
            get {
                return ResourceManager.GetString("i22", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to inşa ettiği yeni kütüphane binası.
        /// </summary>
        public static string i23 {
            get {
                return ResourceManager.GetString("i23", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bir sorunuz mu vardı.
        /// </summary>
        public static string i24 {
            get {
                return ResourceManager.GetString("i24", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Telefon.
        /// </summary>
        public static string i25 {
            get {
                return ResourceManager.GetString("i25", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Faks.
        /// </summary>
        public static string i26 {
            get {
                return ResourceManager.GetString("i26", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sakarya Üniversitesi Merkez Kütüphanesi.
        /// </summary>
        public static string i27 {
            get {
                return ResourceManager.GetString("i27", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Başvuru ve Enformasyon Hizmetleri.
        /// </summary>
        public static string i3 {
            get {
                return ResourceManager.GetString("i3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ödünç Verme Hizmeti.
        /// </summary>
        public static string i4 {
            get {
                return ResourceManager.GetString("i4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fotokopi Hizmeti.
        /// </summary>
        public static string i5 {
            get {
                return ResourceManager.GetString("i5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Süreli Yayınlar Hizmetleri.
        /// </summary>
        public static string i6 {
            get {
                return ResourceManager.GetString("i6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bilgisayar ve Internet Odası.
        /// </summary>
        public static string i7 {
            get {
                return ResourceManager.GetString("i7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Araştırmacı ve kullanıcılara kütüphaneden yararlanma, kaynak sağlama, bibliyografya ve abstraklardan yayın tarama işlemlerinde yardımcı olunmakta ve kullanıcı eğitimi yapılmaktadır..
        /// </summary>
        public static string i8 {
            get {
                return ResourceManager.GetString("i8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sakarya Üniversitesi mensubu olan herkese kütüphaneye üye olmak kaydıyla belirli araliklarla kütüphanede bulunan kitaplar ödünç verilmektedir..
        /// </summary>
        public static string i9 {
            get {
                return ResourceManager.GetString("i9", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İletişim.
        /// </summary>
        public static string iletisim {
            get {
                return ResourceManager.GetString("iletisim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kayıt Tarihi.
        /// </summary>
        public static string kayıt_tarihi {
            get {
                return ResourceManager.GetString("kayıt_tarihi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kütüphane ve Dokümantasyon Dairesi Başkanlığı.
        /// </summary>
        public static string kddb {
            get {
                return ResourceManager.GetString("kddb", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kitaplar.
        /// </summary>
        public static string kitap {
            get {
                return ResourceManager.GetString("kitap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kitaplar.
        /// </summary>
        public static string kitaplar {
            get {
                return ResourceManager.GetString("kitaplar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Konu Başlıkları.
        /// </summary>
        public static string kon_bas {
            get {
                return ResourceManager.GetString("kon_bas", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcı Adı.
        /// </summary>
        public static string kullanıcı_adı {
            get {
                return ResourceManager.GetString("kullanıcı_adı", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Makaleler.
        /// </summary>
        public static string makale {
            get {
                return ResourceManager.GetString("makale", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Makaleler.
        /// </summary>
        public static string makaleler {
            get {
                return ResourceManager.GetString("makaleler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Parola.
        /// </summary>
        public static string parola {
            get {
                return ResourceManager.GetString("parola", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Parola Tekrar.
        /// </summary>
        public static string parola_tek {
            get {
                return ResourceManager.GetString("parola_tek", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Personel.
        /// </summary>
        public static string personel {
            get {
                return ResourceManager.GetString("personel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Raf Numarası.
        /// </summary>
        public static string raf_no {
            get {
                return ResourceManager.GetString("raf_no", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Raf Sırası.
        /// </summary>
        public static string raf_sıra {
            get {
                return ResourceManager.GetString("raf_sıra", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Soyadı.
        /// </summary>
        public static string soyad {
            get {
                return ResourceManager.GetString("soyad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Telefon.
        /// </summary>
        public static string telefon {
            get {
                return ResourceManager.GetString("telefon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tür.
        /// </summary>
        public static string tür {
            get {
                return ResourceManager.GetString("tür", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yayın Bİlgisi.
        /// </summary>
        public static string yay_bil {
            get {
                return ResourceManager.GetString("yay_bil", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yayın Adı.
        /// </summary>
        public static string yayın_adı {
            get {
                return ResourceManager.GetString("yayın_adı", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Hesap Oluştur.
        /// </summary>
        public static string yeni_hesap {
            get {
                return ResourceManager.GetString("yeni_hesap", resourceCulture);
            }
        }
    }
}
